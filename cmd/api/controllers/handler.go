package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/mercadolibre/fury_event-consumer-test/cmd/api/types"
	"log"
	"net/http"
)

func HandleHome(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(writer, "Hello from home")
}

func HandleRoot(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(writer, "hello world!")
}

func PostRequest(writer http.ResponseWriter, request *http.Request) {
	var requestMetaData types.RequestMetaData
	err := json.NewDecoder(request.Body).Decode(&requestMetaData)

	if err != nil {
		fmt.Fprintf(writer, "Error: %v", err)
		return
	}

	log.Printf("Payload %v\n", requestMetaData)
	fmt.Fprintf(writer, "Payload %v\n", requestMetaData)
}

func UserPostRequest(writer http.ResponseWriter, request *http.Request) {
	var user types.User
	err := json.NewDecoder(request.Body).Decode(&user)

	if err != nil {
		fmt.Fprintf(writer, "Error: %v", err)
		return
	}

	response, err := user.ToJson()
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
	}

	log.Printf("Payload %v\n", user)
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(response)
}
