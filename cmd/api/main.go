package main

import (
	"github.com/mercadolibre/fury_event-consumer-test/cmd/api/controllers"
	"github.com/mercadolibre/fury_event-consumer-test/cmd/api/middlewares"
	"github.com/mercadolibre/fury_event-consumer-test/cmd/api/server"
	"log"
	"os"
)

func main() {

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	if err := run(port); err != nil {
		log.Printf("error running server %v\n", err)
	}

}

func run(port string) error {
	var server = server.NewServer(":" + port)
	return addHandlersAndRouters(server).Listen()
}

func addHandlersAndRouters(server *server.Server) *server.Server {
	server.Handle("GET", "/", controllers.HandleRoot)
	server.Handle(
		"POST",
		"/generic-event",
		server.AddMiddleware(controllers.PostRequest, middlewares.Logging()))
	server.Handle(
		"POST",
		"/user",
		server.AddMiddleware(controllers.UserPostRequest, middlewares.Logging()))
	server.Handle(
		"POST",
		"/api",
		server.AddMiddleware(controllers.HandleHome, middlewares.CheckAuth(), middlewares.Logging()))

	return server
}
