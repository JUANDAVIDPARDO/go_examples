package server

import (
	"log"
	"net/http"
	"time"
)

type Router struct {
	rules map[string]map[string]http.HandlerFunc
}

func NewRouter() *Router {
	return &Router{
		rules: make(map[string]map[string]http.HandlerFunc),
	}
}

func (r *Router) FindRouter(method string, path string) (http.HandlerFunc, bool, bool) {
	_, pathExist := r.rules[path]
	handler, methodExist := r.rules[path][method]

	return handler, pathExist, methodExist
}

func (r *Router) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	handler, pathExist, methodExist := r.FindRouter(request.Method, request.URL.Path)

	start := time.Now()

	if !pathExist {
		writer.WriteHeader(http.StatusNotFound)
		log.Println(request.URL.Path, time.Since(start), request.Body, request.Method)
		return
	}

	if !methodExist {
		writer.WriteHeader(http.StatusMethodNotAllowed)
		log.Println(request.URL.Path, time.Since(start), request.Body, request.Method)
		return
	}

	handler(writer, request)
}
