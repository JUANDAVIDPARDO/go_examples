package middlewares

import (
	"fmt"
	"github.com/mercadolibre/fury_event-consumer-test/cmd/api/types"
	"log"
	"net/http"
	"time"
)

func CheckAuth() types.Middleware {
	return func(handlerFunc http.HandlerFunc) http.HandlerFunc {
		return func(writer http.ResponseWriter, request *http.Request) {
			fmt.Fprintf(writer, "Auth Middleware")
			flag := false

			if flag {
				handlerFunc(writer, request)
			} else {
				return
			}
		}
	}
}

func Logging() types.Middleware {
	return func(handlerFunc http.HandlerFunc) http.HandlerFunc {
		return func(writer http.ResponseWriter, request *http.Request) {
			start := time.Now()
			defer func() {
				log.Printf("%s %d %v", request.URL.Path, time.Since(start), request.Body)
			}()
			handlerFunc(writer, request)
		}
	}
}
